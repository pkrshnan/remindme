# RemindMe
A location-based reminders application that reminds you of tasks as you get close to the location. Currently a work in progress, and I apologize for the terrible code, learning Android Development. 

### Copyright
<a href="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</a>
